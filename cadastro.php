<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<title>Ecoescambo</title>
	<link rel="stylesheet" href="css/estilo.css">
</head>
<body>
	
	<div class="cabecalho">
		<?php require 'cabecalho.php'?>
	</div>

	<div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
	</br>
		<div class="container">
			<form class="form-horizontal" action="validacaoFormulario.php" method="POST">
				<fieldset>
					<!-- Form name-->
					<legend>Formulário de Cadastro</legend>

					<!-- Text input-->
					<div class="form-group">
					  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="nome">Nome</label>  
					  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
						<input id="nome" name="nome" placeholder="" class="form-control input-md" required="" type="text">
												
					  </div>
					</div>

					<!-- Text input-->
					<div class="form-group">
					  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="nome">Email</label>  
					  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
						<input id="email" name="email" placeholder="exemplo@ecoescambo.com" class="form-control input-md" >
						<?php
							if(isset($erroEmail))
							{
								echo "$erroEmail";
							}
						?>	
													
					  </div>
					</div>


					<!-- Password input-->
					<div class="form-group">
					  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="email">Senha</label>  
					  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
						<input id="senha" name="senha" placeholder="" class="form-control input-md" type="password">
						<?php
							if(isset($erroSenha))
							{
								echo "$erroSenha";
							}
						?>	
												
					  </div>
					</div>

					<!-- Password input-->
					<div class="form-group">
					  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="login">Confirme a senha</label>  
					  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
						<input id="confirmeSenha" name="confirmeSenha" placeholder="" class="form-control input-md" type="password">
						<?php
							if(isset($erroConfirmeSenha))
							{
								echo "$erroConfirmeSenha";
							}
						?>	
						
					  </div>
					</div>

					<!-- Text input-->
					<div class="form-group">
					  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="data_ts">Telefone</label>  
					  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
						<input id="telefone" name="telefone" placeholder="xx xxxxx-xxxx" class="form-control input-md" type="text">
						<?php
							if(isset($erroTelefone))
							{
								echo "$erroTelefone";
							}
						?>	
												
					  </div>
					</div>

					<!-- Text input-->
					<div class="form-group">
					  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="fantasia">CEP</label>  
					  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
						<input id="cep" name="cep" placeholder="000000000" class="form-control input-md" type="text">
						<?php
							if(isset($erroCep))
							{
								echo "$erroCep";
							}
						?>	
												
					  </div>
					</div>

					<!-- Button (Double) -->
					<div class="form-group">
					  <label class="col-lg-4 col-md-4 col-sm-4 col-xs-4 control-label" for="enviar"></label>
					  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
						<button id="enviar" name="enviar" class="btn btn-success" type="submit">Enviar</button>
						<button id="limpar" name="limpar" class="btn btn-warning" type="reset">Limpar</button>
					  </div>
					</div>

				</fieldset>
			</form>
		</div>
	</div>
	
	<div class = "rodape">
		<?php require 'footer.php'?>
	</div>
</body>
</html>