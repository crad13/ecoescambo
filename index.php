<html>
<head>
		<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<title>Ecoescambo</title>
	<link rel="stylesheet" href="css/estilo.css">

</head>
<body>
	<?php require 'carrega.php'?>	
	<div class="cabecalho">
			<?php require 'cabecalho.php'?>
		</div>

	<div  class="conteudo">
	
		<div clas="prod">
			</br>
			<?php while($rows_produtos = mysqli_fetch_assoc($resultado_produtos)){ ?>
				<div class="col-sm-6 col-md-4">
					<div class="thumbnail label">
						<img class="imgprod" src="<?php echo $rows_produtos['foto'];?>" alt="...">
						<div class="caption">
							<h3><?php echo $rows_produtos['nome'];?></h3>
							<p><a href="descricao.php?id_produto=<?php echo $rows_produtos['id'];?>" class="btn btn-primary" role="button">Detalhes</a></p>
						</div>
					</div>
				</div>
			<?php }?>	
		</div>
	</div>	
	<div class="navega">	
				<nav class="text-center" aria-label="Page navigation">
					<ul class="pagination">
						
					<?php
						for($i=1;$i<$num_pag + 1;$i++ ){ ?>
						<li class="page-item"><a class="page-link" href="index.php?pagina=<?php echo $i;?>"><?php echo $i;?></a></li>
						<?php } ?>

					</ul>
				</nav>
			</div>
	<div class = "rodape">
				<?php require 'footer.php'?>
			</div>

</body>
</html>