<?php

	validaForm();
	
	function validaEmail() {
		
		$email = $_POST['email'];
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			 return "<span class='text-danger'>Email inv&aacute;lido!</span>";			
		}			
	}
	
	function validaSenha(){
		//valicação do formato da senha
		$senha = $_POST['senha'];
		$senhaConfirma  = $_POST['confirmeSenha'];
		
		if (!preg_match("/^.*(?=.{6,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/", $senha)) { 
			return "<span class='text-danger'>Senha deve conter 6 caracteres, combinado o uso de letras com números, e entre as letras caixa baixa e alta</span>";

		} 
        if ($senha == "") 
		{
			return "<span class='text-danger'>Senha não foi alterada!</span>";			
        } 
		else if ($senha != $senhaConfirma) 
		{
			return "<span class='text-danger'> As senhas não conferem</span>";			
        } 	
	}
	
	function validaTelefone(){
		$telefone = $_POST['telefone'];
		if (!preg_match("/.*(?=.{8,9})(?=.*[0-9]).*$/", $telefone)){
			return "<span class='text-danger'>Telefone no formato incorreto.</span>";			
		}
		
	}
	
	function validaCep(){
		$cep = $_POST['cep'];
		if (!preg_match("/.*(?=.{8})(?=.*[0-9]).*$/", $cep)){
			return "<span class='text-danger'>Cep deve conter apenas 8 algarismos.</span>";			
		}
	}
	
	function validaForm()
	{
		$erroEmail = validaEmail();
		$erroSenha = validaSenha();
		$erroTelefone = validaTelefone();
		$erroCep =validaCep();
		if(isset($erroEmail) || isset($erroSenha) || isset($erroTelefone) || isset($erroCep)) 
		{
			return require 'cadastro.php';
		}	
		
		else
		{
			session_start();	
			$_SESSION['usuarioEmail'] = $email;
			$_SESSION['usuarioSenha'] = $senha;
			header('Location: login.php');
		}			
	}
?>