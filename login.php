<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" href="css/estilo.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
	<div class="cabecalho">
		<?php require 'cabecalho.php'?>
	</div>

	<div class="containerlog">		
		<div class="card-container">
		</br>
			<p id="profile-name" class="profile-name-card"></p>
			<form class="form-signin" action="#" method="POST">
				<span id="reauth-email" class="reauth-email"></span>
				<input name="email" id="inputEmail" class="form-control" placeholder="exemplo@ecoescambo.com" autofocus>

				<input type="password" name="senha" id="inputPassword" class="form-control" placeholder="Senha" >
	
				<div id="remember" class="checkbox">
					<label>
						<input type="checkbox" value="remember-me"> Lembrar me
					</label>
				</div>
				<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Entrar</button>
				<?php
					if(isset($erroUser))
					{
						echo "$erroUser";
					}
				?>	
			</form><!-- /form -->
		</div><!-- /card-container -->
	</div><!-- /container -->
	</br>
	<div class = "rodape">
		<?php require 'footer.php'?>
	</div>
</body>
</html>